GuiTabline 0
GuiPopupmenu 0
GuiFont! consolas:h12
GuiLinespace! 2

set guioptions-=T " Remove toolbar
set guioptions-=m " Remove menubar
set guioptions-=r " Remove scrollbar
set guioptions-=L " Remove other scrollbar
set guicursor=n-v-c:block-Cursor/lCursor-blinkon0,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor
set t_Co=256
