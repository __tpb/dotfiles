;; -*- lexical-binding: t -*-

;; https://github.com/DamienCassou/emacs.d/blob/master/init.el
;; https://github.com/abo-abo/oremacs/blob/github/init.el
;; https://github.com/julienfantin/.emacs.d
;; ehttps://github.com/jwiegley/dot-emacs/blob/master/init.el
;; https://github.com/larstvei/dot-emacs
;; https://github.com/thierryvolpiatto/emacs-tv-config
;; http://doc.norang.ca/org-mode.html

(setq package-enable-at-startup nil)

(progn ; `straight'
    (defvar bootstrap-version)
    (let ((bootstrap-file
           (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
          (bootstrap-version 5))
      (unless (file-exists-p bootstrap-file)
        (with-current-buffer
            (url-retrieve-synchronously
             "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
             'silent 'inhibit-cookies)
          (goto-char (point-max))
          (eval-print-last-sexp)))
      (load bootstrap-file nil 'nomessage)
    (straight-use-package 'use-package)
    (setq straight-use-package-by-default t)))


(progn ; `use-package'
  (setq use-package-always-defer t)
  (setq use-package-minimum-reported-time 0)
  (setq use-package-verbose t)
  (setq use-package-compute-statistics t)
  (require 'use-package))

(use-package diminish
  :demand t)

(use-package no-littering
  :demand t)

;; Our real configuration for Org comes much later. Doing this now
;; means that if any packages that are installed in the meantime
;; depend on Org, they will not accidentally cause the Emacs-provided
;; (outdated and duplicated) version of Org to be loaded before the
;; real one is registered.
(straight-use-package 'org)

(use-package recentf
  :defer 10
  :commands (recentf-mode
             recentf-add-file
             recentf-apply-filename-handlers)
  :preface
  (defun recentf-add-dired-directory ()
    (if (and dired-directory
             (file-directory-p dired-directory)
             (not (string= "/" dired-directory)))
        (let ((last-idx (1- (length dired-directory))))
          (recentf-add-file
           (if (= ?/ (aref dired-directory last-idx))
               (substring dired-directory 0 last-idx)
             dired-directory)))))
  :hook (dired-mode . recentf-add-dired-directory)
  :config
  (recentf-mode 1))

(use-package auto-compile
  :demand t
  :init
  (progn
    (setq auto-compile-display-buffer nil)
    (setq auto-compile-mode-line-counter t)
    (setq auto-compile-source-recreate-deletes-dest t)
    (setq auto-compile-toggle-deletes-nonlib-dest t)
    (setq auto-compile-update-autoloads t))
  :hook (auto-compile-inhibit-compile . auto-compile-inhibit-compile-detached-git-head)
  :config
  (progn
    (auto-compile-on-load-mode)
    (auto-compile-on-save-mode)))


(use-package centered-cursor-mode
;;  :diminish
  :demand t
  :after seq
  :commands (global-centered-cursor-mode)
  :config (global-centered-cursor-mode)
  )

(use-package vlf-setup :straight vlf)

(setq package-enable-at-startup nil
      file-name-handler-alist nil
      message-log-max 16384
      gc-cons-threshold 402653184
      gc-cons-percentage 0.6
      auto-window-vscroll nil)

(add-hook 'after-init-hook
          `(lambda ()
             (setq gc-cons-threshold 800000
                   gc-cons-percentage 0.1)
             (garbage-collect)) t)

(if (equal system-type 'windows-nt)
    (setq exec-path (append exec-path '("c:/msys64/mingw64/bin")))
    (setq exec-path (append exec-path '("~/builds/ccls/Release")))
    (setq exec-path (append exec-path '("c:/msys64/mingw64/lib/node_modules/vscode-css-languageserver-bin")))
    (setq exec-path (append exec-path '("c:/msys64/mingw64/lib/node_modules/vscode-html-languageserver-bin"))))
				  
(progn ; `startup'
  (setq inhibit-startup-screen t)
  (setq initial-buffer-choice t)
  (setq initial-major-mode 'text-mode)
  (setq-default indicate-empty-lines t)
  (setq echo-keystrokes 0.1)
  (blink-cursor-mode -1)
  (global-hl-line-mode)
  (electric-pair-mode)
  (setq left-fringe-width 16)
  (setq right-fringe-width 16)
  (setq column-number-mode t)
  (display-time-mode 1)
  (show-paren-mode 1)
  (setq truncate-lines t)
  (setq enable-recursive-minibuffers t)
  (minibuffer-depth-indicate-mode 1)
  (setq initial-scratch-message nil))

(progn ; `files'
  (setq confirm-kill-emacs 'y-or-n-p)
  (setq make-backup-files nil)
  (setq create-lockfiles nil)
  (global-auto-revert-mode 1)
  (prefer-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (setq default-process-coding-system '(utf-8 . utf-8))
  (setq-default buffer-file-coding-system 'utf-8)
  (setq tab-width 4
      indent-tabs-mode nil)
  (setq version-control 'never))

(progn ; `window'
  (bind-key "C-x o" #'other-window)

  (defun my/swap-last-buffers ()
    "Replace currently visible buffer by last one."
    (interactive)
    (switch-to-buffer (other-buffer (current-buffer))))

  (bind-key "C-x B" #'my/swap-last-buffers)

  (defun my/kill-this-buffer ()
    "Kill current buffer.
Better version of `kill-this-buffer' whose docstring says it is
unreliable."
    (interactive)
    (kill-buffer (current-buffer)))

  (bind-key "C-x k" #'my/kill-this-buffer))

  (progn ; `map-ynp'
  ;; Make all "yes or no" prompts show "y or n" instead
  (setq read-answer-short t)
  (fset 'yes-or-no-p 'y-or-n-p))

  (use-package custom
  :straight nil
  :demand t
  :config
  (progn
    (setq custom-file (no-littering-expand-etc-file-name "custom.el"))
    (when (file-exists-p custom-file)
      (load custom-file))))

  (use-package frame
  :straight nil
  :bind (("C-x C-z" . my/suspend-on-tty-only))
  :config
  (progn
    (scroll-bar-mode -1)
    (tool-bar-mode -1)
    (menu-bar-mode -1)
    (defun my/set-selected-frame-dark ()
      "Make current frame use GTK dark theme."
      (interactive)
      (let ((frame-name (cdr (assq 'name (frame-parameters (selected-frame))))))
        (call-process-shell-command
         (format
          "xprop -f _GTK_THEME_VARIANT 8u -set _GTK_THEME_VARIANT 'dark' -name '%s'"
          frame-name))))))

  (progn ; `theme'
  (defvar tpb-theme-directory "~/.emacs.d/themes/")
  (setq custom-theme-directory tpb-theme-directory))
    
(use-package doom-themes
  :config (doom-themes-org-config)
  :custom (doom-cobalt2-colorful-headers t))
    
    (defun my/setup-frame (&optional frame)
      "Configure look of FRAME.
If FRAME is nil, configure current frame. If non-nil, make FRAME
current."
      (when frame (select-frame frame))
      (setq frame-title-format '(buffer-file-name "%f" ("%b")))
      (when (window-system)
        (ignore-errors
          (load-theme 'doom-vscode-dark t)
        (my/set-selected-frame-dark))))

    (add-to-list 'default-frame-alist '(inhibit-double-buffering . t))

    (if (daemonp)
        (add-hook 'after-make-frame-functions #'my/setup-frame))
      (my/setup-frame)

(progn ; `ora-fonts'
 (defun ora-set-font (&optional frame)
  (when frame
    (select-frame frame))
  (condition-case nil
      (set-frame-font
       "consolas")
    (error
     (set-frame-font
      "source code pro"))))
  (ora-set-font)
  (set-face-attribute 'default nil :height (if (eq system-type 'windows-nt) 113 113))
  (set-fontset-font t nil "Symbola" nil 'append)
  (add-hook 'after-make-frame-functions 'ora-set-font))

 (use-package display-line-numbers
   :straight nil
   :init (defun tpb-num-hook ()
	       (display-line-numbers-mode)
	       (setq display-line-numbers 'relative))
   :hook ((prog-mode conf-mode org-mode text-mode) . tpb-num-hook)
  )

(use-package uniquify
  :straight nil
    :init
  (setq uniquify-buffer-name-style 'reverse)
  (setq uniquify-separator "/")
  (setq uniquify-ignore-buffers-re "^\\*"))

(use-package ace-window
  :bind ("M-o" . ace-window))

(use-package spaceline
  :demand t
  :config (spaceline-emacs-theme)
          (spaceline-helm-mode))

(use-package helm-config
    :straight helm
  :bind
  (("C-x C-f" . helm-find-files)
   ("C-x C-b" . helm-buffers-list)
   ("C-x C-r" . helm-recentf)
   ("C-x b"   . helm-mini)
   ("M-x"     . helm-M-x)
   ("M-y"     . helm-show-kill-ring)
   ("C-h a"   . helm-apropos)
   ("C-c h"   . helm-command-prefix)
   ("C-h C-l" . helm-locate-library))
  :preface (defvar helm-command-prefix-key "C-c h")
  :config
  (progn
    ;; Force vertical split at bottom
    (helm-autoresize-mode 1)
    ;; Remap persistent action to TAB
    (bind-keys
     :map helm-map
     ("C-z" . helm-select-action)
     ("<tab>" . helm-execute-persistent-action)
     ("C-i" . helm-execute-persistent-action))
    ;; Remove '..' in helm-find-files
    ;; Seems to breaks with recent version...
    ;; (advice-add
    ;;  'helm-ff-filter-candidate-one-by-one
    ;;  :around (lambda (fcn file)
    ;;            (unless (string-match "\\(?:/\\|\\`\\)\\.\\{2\\}\\'" file)
    ;;              (funcall fcn file))))
    )
  :custom
  (helm-split-window-in-side-p t)
  (helm-split-window-default-side 'below)
  (helm-autoresize-min-height 40)
  (helm-autoresize-max-height 40)
  (helm-idle-delay 0.0)
  (helm-input-idle-delay 0.01)
  (helm-echo-input-in-header-line t)
  (helm-candidate-number-limit 100)
  (helm-ff-fuzzy-matching t)
  (helm-apropos-fuzzy-match t)
  (helm-buffers-fuzzy-matching t)
  (helm-locate-fuzzy-match nil)
  (helm-mode-fuzzy-match t)
  (helm-recentf-fuzzy-match t))

(use-package helm-command
  :straight nil
  :after helm
  :commands (helm-M-x)
  :custom
  (helm-M-x-always-save-history t)
  (helm-M-x-fuzzy-match t))

(use-package helm-mode
  :diminish helm-mode
  :straight nil
  :after helm
  :init (helm-mode)
  :custom
  (helm-input-idle-delay 0.01)
  (helm-ff-transformer-show-only-basename t)
  (helm-ff-file-name-history-use-recentf t)
  (helm-ff-skip-boring-files nil))

(use-package helm-swoop
  :bind ("C-s" . helm-swoop))

(use-package helm-descbinds
  :bind ("C-h b" . helm-descbinds)
  :init
  (fset 'describe-bindings 'helm-descbinds))

(use-package helm-describe-modes
  :after helm
  :bind ("C-h m" . helm-describe-modes))

(use-package helm-font
  :straight nil
  :commands (helm-select-xfont helm-ucs))

(use-package expand-region
  :bind ("C-=" . er/expand-region))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :hook web-mode)

(use-package phi-search
  :defer 5)

(use-package phi-search-mc
  :after (phi-search multiple-cursors)
  :config
  (phi-search-mc/setup-keys)
  (add-hook 'isearch-mode-mode #'phi-search-from-isearch-mc/setup-keys))

(use-package selected
  :demand t
  :diminish
  :bind (:map selected-keymap
              ("[" . align-code)
              ("f" . fill-region)
              ("U" . unfill-region)
              ("d" . downcase-region)
              ("u" . upcase-region)
              ("r" . reverse-region)
              ("s" . sort-lines))
  :config
  (selected-global-mode 1))

(define-key input-decode-map [?\C-m] [C-m])

(eval-and-compile
  (mapc #'(lambda (entry)
            (define-prefix-command (cdr entry))
            (bind-key (car entry) (cdr entry)))
        '(("C-,"   . my-ctrl-comma-map)
          ("<C-m>" . my-ctrl-m-map))))

(use-package multiple-cursors
  :after phi-search selected
  :defer 5

  ;; - Sometimes you end up with cursors outside of your view. You can scroll
  ;;   the screen to center on each cursor with `C-v` and `M-v`.
  ;;
  ;; - If you get out of multiple-cursors-mode and yank - it will yank only
  ;;   from the kill-ring of main cursor. To yank from the kill-rings of every
  ;;   cursor use yank-rectangle, normally found at C-x r y.

  :bind (("<C-m> ^"     . mc/edit-beginnings-of-lines)
         ("<C-m> `"     . mc/edit-beginnings-of-lines)
         ("<C-m> $"     . mc/edit-ends-of-lines)
         ("<C-m> '"     . mc/edit-ends-of-lines)
         ("<C-m> R"     . mc/reverse-regions)
         ("<C-m> S"     . mc/sort-regions)
         ("<C-m> W"     . mc/mark-all-words-like-this)
         ("<C-m> Y"     . mc/mark-all-symbols-like-this)
         ("<C-m> a"     . mc/mark-all-like-this-dwim)
         ("<C-m> c"     . mc/mark-all-dwim)
         ("<C-m> l"     . mc/insert-letters)
         ("<C-m> n"     . mc/insert-numbers)
         ("<C-m> r"     . mc/mark-all-in-region)
         ("<C-m> s"     . set-rectangular-region-anchor)
         ("<C-m> %"     . mc/mark-all-in-region-regexp)
         ("<C-m> t"     . mc/mark-sgml-tag-pair)
         ("<C-m> w"     . mc/mark-next-like-this-word)
         ("<C-m> x"     . mc/mark-more-like-this-extended)
         ("<C-m> y"     . mc/mark-next-like-this-symbol)
         ("<C-m> C-x"   . reactivate-mark)
         ("<C-m> C-SPC" . mc/mark-pop)
         ("<C-m> ("     . mc/mark-all-symbols-like-this-in-defun)
         ("<C-m> C-("   . mc/mark-all-words-like-this-in-defun)
         ("<C-m> M-("   . mc/mark-all-like-this-in-defun)
         ("<C-m> ["     . mc/vertical-align-with-space)
         ("<C-m> {"     . mc/vertical-align)

         ("S-<down-mouse-1>")
         ("S-<mouse-1>" . mc/add-cursor-on-click))

  :bind (:map selected-keymap
              ("c"   . mc/edit-lines)
              ("w"   . mc/mark-next-word-like-this)
              ("Y"   . mc/mark-previous-symbol-like-this)
              ("y"   . mc/mark-next-symbol-like-this)
              ("C-<" . mc/skip-to-previous-like-this)
              (">"   . mc/unmark-previous-like-this)
              (","   . mc/mark-previous-like-this)
              ("C->" . mc/skip-to-next-like-this)
              ("<"   . mc/unmark-next-like-this)
              ("."   . mc/mark-next-like-this)
              ("W"   . mc/mark-previous-word-like-this))

  :preface
  (defun reactivate-mark ()
    (interactive)
    (activate-mark)))

(use-package crux)
(use-package avy
  :bind* ("C-." . avy-goto-char-timer)
  :config
  (avy-setup-default))

(use-package avy-zap
  :bind (("M-z" . avy-zap-to-char-dwim)
         ("M-Z" . avy-zap-up-to-char-dwim)))
(use-package company
  :demand t
  :diminish company
  :init   (defvar radian--company-backends-global
    '(company-capf
      company-files
      (company-dabbrev-code company-keywords)
      company-dabbrev)
    "Values for `company-backends' used everywhere.
If `company-backends' is overridden by Radian, then these
backends will still be included.")
  :config (progn
          (setq company-idle-delay 0)
          (setq company-frontends '(company-pseudo-tooltip-frontend))
          (setq company-show-numbers t)
	  (setq company-auto-complete-chars nil)
	  (setq company-dabbrev-other-buffers nil)
          (setq company-minimum-prefix-length 3)
          (setq company-dabbrev-ignore-case nil)
          (setq company-dabbrev-downcase nil)
          (setq company-tooltip-align-annotations t)
	  (global-company-mode))
  )

(use-package company-prescient
  :demand t
  :after company
  :config

  ;; Use `prescient' for Company menus.
  (company-prescient-mode +1))

(use-package helm-flyspell
  :bind ("C-;" . helm-flyspell-correct))

(use-package which-key
  :demand t
  :config (which-key-mode 1)
  (which-key-add-key-based-replacements
    "C-c p" "projectile-command-prefix")
  :custom  (which-key-idle-delay 0.2))

(use-package treemacs
    :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
	("C-x t p"   . treemacs-projectile))
  :custom (treemacs-no-png-images t)
  (treemacs-width 35)
  ) 

(use-package projectile
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :config (projectile-mode)
  :custom (projectile-completion-system 'helm))

(use-package treemacs-projectile
  :after treemacs projectile)

(use-package treemacs-magit
  :after treemacs magit)

(use-package lsp-treemacs
  :after treemacs lsp-mode)

(use-package yasnippet
  :demand t
  :config (yas-global-mode 1))

(use-package lsp-mode
  :after yasnippet
  :hook ((html-mode css-mode scss-mode c++-mode c-mode) . lsp)
  :config  (setq lsp-prefer-flymake nil)
  )

(use-package lsp-ui
    :hook lsp-mode)
 
(use-package flycheck
  :hook lsp-mode)

(use-package company-lsp
  :config (push 'company-lsp company-backends))

  (setq org-modules '(org-bbdb
                      org-gnus
                      org-drill
                      org-info
                      org-jsinfo
                      org-habit
                      org-irc
                      org-mouse
                      org-protocol
                      org-annotate-file
                      org-eval
                      org-expiry
                      org-interactive-query
                      org-man
                      org-collector
                      org-panel
                      org-screen
                      org-toc))
(eval-after-load 'org
 '(org-load-modules-maybe t))
;; Prepare stuff for org-export-backends
(setq org-export-backends '(org latex icalendar html ascii))

(use-package org
    :bind* (;; Add the global keybindings for accessing Org Agenda and
          ;; Org Capture that are recommended in the Org manual.
          ("C-c a" . org-agenda)
	  ("C-c l" . org-store-link)
	  ("<f12>" . org-agenda)
	  ("<f5>" . bh/org-todo)
	  ("<S-f5>" . bh/widen)
	  ("<f7>" . bh/set-truncate-lines)
	  ("<f8>" . org-cycle-agenda-files)
	  ("<f9> <f9>" . bh/show-org-agenda)
	  ("<f9> b" . bbdb)
	  ("<f9> c" . calendar)
	  ("<f9> f" . boxquote-insert-file)
	  ("<f9> g" . gnus)
	  ("<f9> h" . bh/hide-other)
	  ("<f9> n" . bh/toggle-next-task-display)

	  ("<f9> I" . bh/punch-in)
	  ("<f9> O" . bh/punch-out)

	  ("<f9> o" . bh/make-org-scratch)

	  ("<f9> r" . boxquote-region)
	  ("<f9> s" . bh/switch-to-scratch)

	  ("<f9> t" . bh/insert-inactive-timestamp)
	  ("<f9> T" . bh/toggle-insert-inactive-timestamp)

	  ("<f9> v" . visible-mode)
	  ("<f9> l" . org-toggle-link-display)
	  ("<f9> SPC" . bh/clock-in-last-task)
	  ("C-<f9>" . previous-buffer)
	  ("M-<f9>" . org-toggle-inline-images)
	  ("C-x n r" . narrow-to-region)
	  ("C-<f10>" . next-buffer)
	  ("<f11>" . org-clock-goto)
	  ("C-<f11>" . org-clock-in)
	  ("C-s-<f12>" . bh/save-then-publish)
	  ("C-c c" . org-capture)
	  )
    :config (progn
	      (setq org-startup-folded 'content)
	      (setq org-agenda-sticky t)
	      (setq org-stuck-projects (quote ("" nil nil "")))
              (setq org-enforce-todo-dependencies t)
	      (setq org-special-ctrl-a/e t)
	      (setq org-special-ctrl-k t)
	      (setq org-agenda-repeating-timestamp-show-all t)
	      (setq org-agenda-show-all-dates t)
	      (setq org-agenda-start-on-weekday 1)
	      (defun bh/agenda-sort (a b)
  "Sorting strategy for agenda items.
Late deadlines first, then scheduled, then non-late deadlines"
  (let (result num-a num-b)
    (cond
     ; time specific items are already sorted first by org-agenda-sorting-strategy

     ; non-deadline and non-scheduled items next
     ((bh/agenda-sort-test 'bh/is-not-scheduled-or-deadline a b))

     ; deadlines for today next
     ((bh/agenda-sort-test 'bh/is-due-deadline a b))

     ; late deadlines next
     ((bh/agenda-sort-test-num 'bh/is-late-deadline '> a b))

     ; scheduled items for today next
     ((bh/agenda-sort-test 'bh/is-scheduled-today a b))

     ; late scheduled items next
     ((bh/agenda-sort-test-num 'bh/is-scheduled-late '> a b))

     ; pending deadlines last
     ((bh/agenda-sort-test-num 'bh/is-pending-deadline '< a b))

     ; finally default to unsorted
     (t (setq result nil)))
    result))
	      (defun bh/is-not-scheduled-or-deadline (date-str)
  (and (not (bh/is-deadline date-str))
       (not (bh/is-scheduled date-str))))

(defun bh/is-due-deadline (date-str)
  (string-match "Deadline:" date-str))

(defun bh/is-late-deadline (date-str)
  (string-match "\\([0-9]*\\) d\. ago:" date-str))

(defun bh/is-pending-deadline (date-str)
  (string-match "In \\([^-]*\\)d\.:" date-str))

(defun bh/is-deadline (date-str)
  (or (bh/is-due-deadline date-str)
      (bh/is-late-deadline date-str)
      (bh/is-pending-deadline date-str)))

(defun bh/is-scheduled (date-str)
  (or (bh/is-scheduled-today date-str)
      (bh/is-scheduled-late date-str)))

(defun bh/is-scheduled-today (date-str)
  (string-match "Scheduled:" date-str))

(defun bh/is-scheduled-late (date-str)
  (string-match "Sched\.\\(.*\\)x:" date-str))
	      (setq org-highlight-sparse-tree-matches nil)
	      (setq org-use-fast-todo-selection t)
	      (setq org-treat-S-cursor-todo-selection-as-state-change nil)
	      (setq org-refile-targets (quote ((nil :maxlevel . 9)
					       (org-agenda-files :maxlevel . 9))))
	      (setq org-refile-use-outline-path t)
	      (setq org-completion-use-helm t)
	      (setq org-refile-allow-creating-parent-nodes (quote confirm))
	      (defun bh/verify-refile-target ()
		"Exclude todo keywords with a done state from refile targets"
		(not (member (nth 2 (org-heading-components)) org-done-keywords)))
	      (setq org-refile-target-verify-function 'bh/verify-refile-target)
	      )
    (progn
      (setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "APPOINTMENT(a@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING")
	      (sequence "SOMEDAY(s@/!)" "|" "CANCELLED(c@/!)"))))
      )
    )
(use-package org-agenda
  :straight nil
  :config (progn
	    (setq org-default-notes-file "~/projects/org-files/inbox.org")
	    (setq org-agenda-dim-blocked-tasks t)
	    (setq org-agenda-files (quote ("~/projects/org-files")))
	    (setq org-deadline-warning-days 30)
	    (setq org-habit-graph-column 50)
	    (setq org-agenda-sorting-strategy
      (quote ((agenda habit-down time-up user-defined-up effort-up category-keep)
              (todo category-up effort-up)
              (tags category-up effort-up)
              (search category-up))))
	    (setq org-capture-templates
  		  (quote (("t" "todo" entry (file "~/projects/org-files/inbox.org")
			   "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
			  ("r" "respond" entry (file "~/projects/org-files/inbox.org")
			   "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
			  ("n" "note" entry (file "~/projects/org-files/inbox.org")
			   "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
			  ("j" "Journal" entry (file+datetree "~/projects/org-files/diary.org")
			   "* %?\n%U\n" :clock-in t :clock-resume t)
			  ("w" "org-protocol" entry (file "~/projects/org-files/inbox.org")
			   "* TODO Review %c\n%U\n" :immediate-finish t)
			  ("m" "Meeting" entry (file "~/projects/org-files/inbox.org")
			   "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
			  ("p" "Phone call" entry (file "~/projects/org-files/inbox.org")
			   "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
			  ("h" "Habit" entry (file "~/projects/org-files/inbox.org")
			   "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))
	    )
  (progn
    ;; Compact the block agenda view
(setq org-agenda-compact-blocks t)

;; Custom agenda command definitions
(setq org-agenda-custom-commands
      (quote (("N" "Notes" tags "NOTE"
               ((org-agenda-overriding-header "Notes")
                (org-tags-match-list-sublevels t)))
              ("h" "Habits" tags-todo "STYLE=\"habit\""
               ((org-agenda-overriding-header "Habits")
                (org-agenda-sorting-strategy
                 '(todo-state-down effort-up category-keep))))
              (" " "Agenda"
               ((agenda "" nil)
                (tags "REFILE"
                      ((org-agenda-overriding-header "Tasks to Refile")
                       (org-tags-match-list-sublevels nil)))
                (tags-todo "-CANCELLED-SOMEDAY/!"
                           ((org-agenda-overriding-header "Stuck Projects")
                            (org-agenda-skip-function 'bh/skip-non-stuck-projects)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-HOLD-CANCELLED-SOMEDAY/!"
                           ((org-agenda-overriding-header "Projects")
                            (org-agenda-skip-function 'bh/skip-non-projects)
                            (org-tags-match-list-sublevels 'indented)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-CANCELLED-SOMEDAY/!NEXT"
                           ((org-agenda-overriding-header (concat "Project Next Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                            (org-tags-match-list-sublevels t)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(todo-state-down effort-up category-keep))))
                (tags-todo "-REFILE-CANCELLED-SOMEDAY-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Project Subtasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-REFILE-CANCELLED-SOMEDAY-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Standalone Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-CANCELLED+WAITING|HOLD|SOMEDAY/!"
                           ((org-agenda-overriding-header (concat "Waiting and Postponed Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-tasks)
                            (org-tags-match-list-sublevels nil)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)))
                (tags "-REFILE/"
                      ((org-agenda-overriding-header "Tasks to Archive")
                       (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                       (org-tags-match-list-sublevels nil))))
               nil))))
)
  (progn
    (defun bh/is-project-p ()
  "Any task with a todo keyword subtask"
  (save-restriction
    (widen)
    (let ((has-subtask)
          (subtree-end (save-excursion (org-end-of-subtree t)))
          (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
      (save-excursion
        (forward-line 1)
        (while (and (not has-subtask)
                    (< (point) subtree-end)
                    (re-search-forward "^\*+ " subtree-end t))
          (when (member (org-get-todo-state) org-todo-keywords-1)
            (setq has-subtask t))))
      (and is-a-task has-subtask))))

(defun bh/is-project-subtree-p ()
  "Any task with a todo keyword that is in a project subtree.
Callers of this function already widen the buffer view."
  (let ((task (save-excursion (org-back-to-heading 'invisible-ok)
                              (point))))
    (save-excursion
      (bh/find-project-task)
      (if (equal (point) task)
          nil
        t))))

(defun bh/is-task-p ()
  "Any task with a todo keyword and no subtask"
  (save-restriction
    (widen)
    (let ((has-subtask)
          (subtree-end (save-excursion (org-end-of-subtree t)))
          (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
      (save-excursion
        (forward-line 1)
        (while (and (not has-subtask)
                    (< (point) subtree-end)
                    (re-search-forward "^\*+ " subtree-end t))
          (when (member (org-get-todo-state) org-todo-keywords-1)
            (setq has-subtask t))))
      (and is-a-task (not has-subtask)))))

(defun bh/is-subproject-p ()
  "Any task which is a subtask of another project"
  (let ((is-subproject)
        (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
    (save-excursion
      (while (and (not is-subproject) (org-up-heading-safe))
        (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
          (setq is-subproject t))))
    (and is-a-task is-subproject)))

(defun bh/list-sublevels-for-projects-indented ()
  "Set org-tags-match-list-sublevels so when restricted to a subtree we list all subtasks.
  This is normally used by skipping functions where this variable is already local to the agenda."
  (if (marker-buffer org-agenda-restrict-begin)
      (setq org-tags-match-list-sublevels 'indented)
    (setq org-tags-match-list-sublevels nil))
  nil)

(defun bh/list-sublevels-for-projects ()
  "Set org-tags-match-list-sublevels so when restricted to a subtree we list all subtasks.
  This is normally used by skipping functions where this variable is already local to the agenda."
  (if (marker-buffer org-agenda-restrict-begin)
      (setq org-tags-match-list-sublevels t)
    (setq org-tags-match-list-sublevels nil))
  nil)

(defvar bh/hide-scheduled-and-waiting-next-tasks t)

(defun bh/toggle-next-task-display ()
  (interactive)
  (setq bh/hide-scheduled-and-waiting-next-tasks (not bh/hide-scheduled-and-waiting-next-tasks))
  (when  (equal major-mode 'org-agenda-mode)
    (org-agenda-redo))
  (message "%s WAITING and SCHEDULED NEXT Tasks" (if bh/hide-scheduled-and-waiting-next-tasks "Hide" "Show")))

(defun bh/skip-stuck-projects ()
  "Skip trees that are not stuck projects"
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (if (bh/is-project-p)
          (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
                 (has-next ))
            (save-excursion
              (forward-line 1)
              (while (and (not has-next) (< (point) subtree-end) (re-search-forward "^\\*+ NEXT " subtree-end t))
                (unless (member "WAITING" (org-get-tags))
                  (setq has-next t))))
            (if has-next
                nil
              next-headline)) ; a stuck project, has subtasks but no next task
        nil))))

(defun bh/skip-non-stuck-projects ()
  "Skip trees that are not stuck projects"
  ;; (bh/list-sublevels-for-projects-indented)
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (if (bh/is-project-p)
          (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
                 (has-next ))
            (save-excursion
              (forward-line 1)
              (while (and (not has-next) (< (point) subtree-end) (re-search-forward "^\\*+ NEXT " subtree-end t))
                (unless (member "WAITING" (org-get-tags))
                  (setq has-next t))))
            (if has-next
                next-headline
              nil)) ; a stuck project, has subtasks but no next task
        next-headline))))

(defun bh/skip-non-projects ()
  "Skip trees that are not projects"
  ;; (bh/list-sublevels-for-projects-indented)
  (if (save-excursion (bh/skip-non-stuck-projects))
      (save-restriction
        (widen)
        (let ((subtree-end (save-excursion (org-end-of-subtree t))))
          (cond
           ((bh/is-project-p)
            nil)
           ((and (bh/is-project-subtree-p) (not (bh/is-task-p)))
            nil)
           (t
            subtree-end))))
    (save-excursion (org-end-of-subtree t))))

(defun bh/skip-non-tasks ()
  "Show non-project tasks.
Skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((bh/is-task-p)
        nil)
       (t
        next-headline)))))

(defun bh/skip-project-trees-and-habits ()
  "Skip trees that are projects"
  (save-restriction
    (widen)
    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       ((org-is-habit-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-projects-and-habits-and-single-tasks ()
  "Skip trees that are projects, tasks that are habits, single non-project tasks"
  (save-restriction
    (widen)
    (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((org-is-habit-p)
        next-headline)
       ((and bh/hide-scheduled-and-waiting-next-tasks
             (member "WAITING" (org-get-tags)))
        next-headline)
       ((bh/is-project-p)
        next-headline)
       ((and (bh/is-task-p) (not (bh/is-project-subtree-p)))
        next-headline)
       (t
        nil)))))

(defun bh/skip-project-tasks-maybe ()
  "Show tasks related to the current restriction.
When restricted to a project, skip project and sub project tasks, habits, NEXT tasks, and loose tasks.
When not restricted, skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
           (next-headline (save-excursion (or (outline-next-heading) (point-max))))
           (limit-to-project (marker-buffer org-agenda-restrict-begin)))
      (cond
       ((bh/is-project-p)
        next-headline)
       ((org-is-habit-p)
        subtree-end)
       ((and (not limit-to-project)
             (bh/is-project-subtree-p))
        subtree-end)
       ((and limit-to-project
             (bh/is-project-subtree-p)
             (member (org-get-todo-state) (list "NEXT")))
        subtree-end)
       (t
        nil)))))

(defun bh/skip-project-tasks ()
  "Show non-project tasks.
Skip project and sub-project tasks, habits, and project related tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       ((org-is-habit-p)
        subtree-end)
       ((bh/is-project-subtree-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-non-project-tasks ()
  "Show project tasks.
Skip project and sub-project tasks, habits, and loose non-project tasks."
  (save-restriction
    (widen)
    (let* ((subtree-end (save-excursion (org-end-of-subtree t)))
           (next-headline (save-excursion (or (outline-next-heading) (point-max)))))
      (cond
       ((bh/is-project-p)
        next-headline)
       ((org-is-habit-p)
        subtree-end)
       ((and (bh/is-project-subtree-p)
             (member (org-get-todo-state) (list "NEXT")))
        subtree-end)
       ((not (bh/is-project-subtree-p))
        subtree-end)
       (t
        nil)))))

(defun bh/skip-projects-and-habits ()
  "Skip trees that are projects and tasks that are habits"
  (save-restriction
    (widen)
    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
      (cond
       ((bh/is-project-p)
        subtree-end)
       ((org-is-habit-p)
        subtree-end)
       (t
        nil)))))

(defun bh/skip-non-subprojects ()
  "Skip trees that are not projects"
  (let ((next-headline (save-excursion (outline-next-heading))))
    (if (bh/is-subproject-p)
        nil
      next-headline)))
(defun bh/find-project-task ()
  "Move point to the parent (project) task if any"
  (save-restriction
    (widen)
    (let ((parent-task (save-excursion (org-back-to-heading 'invisible-ok) (point))))
      (while (org-up-heading-safe)
        (when (member (nth 2 (org-heading-components)) org-todo-keywords-1)
          (setq parent-task (point))))
      (goto-char parent-task)
      parent-task)))
)
  )
(use-package org-checklist
  :straight nil)
